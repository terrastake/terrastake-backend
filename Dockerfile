# Use the official ASP.NET Core runtime as a parent image
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

# Use the SDK image to build the application
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
# Copy all necessary project files
COPY ["TerraStake.Api/TerraStake.Api.csproj", "TerraStake.Api/"]
COPY ["TerraStake.Application/TerraStake.Application.csproj", "TerraStake.Application/"]
COPY ["TerraStake.Infrastructure/TerraStake.Infrastructure.csproj", "TerraStake.Infrastructure/"]
COPY ["TerraStake.Persistence/TerraStake.Persistence.csproj", "TerraStake.Persistence/"]
COPY ["TerraStake.Domain/TerraStake.Domain.csproj", "TerraStake.Domain/"]

RUN dotnet restore "TerraStake.Api/TerraStake.Api.csproj"
COPY . .
WORKDIR "/src/TerraStake.Api"
RUN dotnet build "TerraStake.Api.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "TerraStake.Api.csproj" -c $BUILD_CONFIGURATION  -o /app/publish

# Final stage/image
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "TerraStake.Api.dll"]

