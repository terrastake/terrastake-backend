﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TerraStake.Application.User;

namespace TerraStake.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<List<UserResponseModel>> GetAll(CancellationToken cancellationToken)
        {
            return await _userService.GetAll(cancellationToken);
        }

        [HttpGet("{id}")]
        public async Task<UserResponseModel> Get(CancellationToken cancellationToken, int id)
        {
            return await _userService.Get(cancellationToken, id);
        }

        [HttpDelete("{id}")]
        public async Task Delete(CancellationToken cancellationToken, int id)
        {
            await _userService.Delete(cancellationToken, id);
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task Post(CancellationToken cancellationToken, UserCreateModel request)
        {
            await _userService.CreateAsync(cancellationToken, request);
        }

        [HttpPut]
        public async Task Put(CancellationToken cancellationToken, UserRequestModel request)
        {
            await _userService.Update(cancellationToken, request);
        }

    }
}
