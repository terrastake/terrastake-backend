﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TerraStake.Application.Project;
using TerraStake.Application.User;

namespace TerraStake.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<List<ProjectResponseModel>> GetAll(CancellationToken cancellationToken)
        {
            return await _projectService.GetAll(cancellationToken);
        }

        [HttpGet("{id}")]
        public async Task<ProjectResponseModel> Get(CancellationToken cancellationToken, int id)
        {
            return await _projectService.Get(cancellationToken, id);
        }

        [HttpDelete("{id}")]
        public async Task Delete(CancellationToken cancellationToken, int id)
        {
            await _projectService.Delete(cancellationToken, id);
        }

        [HttpPost("Register")]
        public async Task Post(CancellationToken cancellationToken, ProjectRequestModel request)
        {
            await _projectService.CreateAsync(cancellationToken, request);
        }

        [HttpPut]
        public async Task Put(CancellationToken cancellationToken, ProjectRequestModel request)
        {
            await _projectService.Update(cancellationToken, request);
        }

    }
}

