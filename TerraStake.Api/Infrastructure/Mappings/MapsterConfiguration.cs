﻿using Mapster;
using TerraStake.Application.Project;
using TerraStake.Application.User;
using TerraStake.Domain.Project;
using TerraStake.Domain.User;

namespace TerraStake.Api.Infrastructure.Mappings
{
    public static class MapsterConfiguration
    {
        public static void RegisterMaps(this IServiceCollection services)
        {
            TypeAdapterConfig<UserRequestModel, UserEntity>
            .NewConfig()
            .TwoWays();

            TypeAdapterConfig<UserRequestModel, UserResponseModel>
            .NewConfig()
            .TwoWays();

            TypeAdapterConfig<ProjectRequestModel, ProjectEntity>
           .NewConfig()
           .TwoWays();

            TypeAdapterConfig<ProjectRequestModel, ProjectResponseModel>
            .NewConfig()
            .TwoWays();
        }
    }
}

