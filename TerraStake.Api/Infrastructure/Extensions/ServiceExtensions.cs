﻿using TerraStake.Application.Project;
using TerraStake.Application.User;
using TerraStake.Infrastructure.Project;
using TerraStake.Infrastructure.User;

namespace TerraStake.Api.Infrastructure.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProjectService, ProjectService>();
        }

        public static void AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
        }
    }
}