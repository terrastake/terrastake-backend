﻿using TerraStake.Domain.User;

namespace TerraStake.Domain.Project
{
    public class ProjectEntity
    {
        public int ProjectId { get; set; }

        public string Description { get; set; }

        public decimal Cost { get; set; }

        public string Location { get; set; }

        public string Name { get; set; }

        public float Area { get; set; }

        public List<string> Images { get; set; }

        // Navigation property for the owner of the project
        public UserEntity Owner { get; set; }

        public int OwnerId { get; set; }

        // Navigation property for the investor of the project
        public List<UserEntity> Investors { get; set; }
    }
}
