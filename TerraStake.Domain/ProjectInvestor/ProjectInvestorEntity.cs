﻿using TerraStake.Domain.Project;
using TerraStake.Domain.User;

namespace TerraStake.Domain.ProjectInvestor
{
    public class ProjectInvestorEntity
    {
        public int ProjectId { get; set; }
        public ProjectEntity Project { get; set; }

        public int UserId { get; set; }
        public UserEntity User { get; set; }
    }
}