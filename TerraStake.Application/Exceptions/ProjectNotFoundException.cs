﻿namespace TerraStake.Application.Exceptions
{
    public class ProjectNotFoundException : Exception
    {

        public string Code = "ProjectNotFound";

        public ProjectNotFoundException(string message) : base(message) { }

    }
}
