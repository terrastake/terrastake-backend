﻿using TerraStake.Application.User;

namespace TerraStake.Application.Project
{
    public interface IProjectService
    {
        Task CreateAsync(CancellationToken cancellationToken, ProjectRequestModel project);
        Task<List<ProjectResponseModel>> GetAll(CancellationToken cancellationToken);
        Task<ProjectResponseModel> Get(CancellationToken cancellationToken, int id);
        Task Update(CancellationToken cancellationToken, ProjectRequestModel project);
        Task Delete(CancellationToken cancellationToken, int id);
    }
}