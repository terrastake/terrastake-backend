﻿using System.Linq.Expressions;
using TerraStake.Domain.Project;
using TerraStake.Domain.User;

namespace TerraStake.Application.Project
{
    public interface IProjectRepository
    {
        Task<List<ProjectEntity>> GetAllAsync(CancellationToken cancellationToken);
        Task<List<ProjectEntity>> GetAllAsync(CancellationToken token, Expression<Func<ProjectEntity, bool>> func);
        Task<ProjectEntity> GetAsync(CancellationToken cancellationToken, int id);
        Task UpdateAsync(CancellationToken cancellationToken, ProjectEntity project);
        Task DeleteAsync(CancellationToken cancellationToken, int id);
        Task<bool> Exists(CancellationToken cancellationToken, int id);
        Task CreateAsync(CancellationToken cancellationToken, ProjectEntity project);
    }
}
