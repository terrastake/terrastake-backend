﻿using TerraStake.Application.User;

namespace TerraStake.Application.Project
{
    public class ProjectResponseModel
    {
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public float Area { get; set; }

        public List<string> Images { get; set; }

        public decimal Cost { get; set; }

        public int OwnerId { get; set; }

        public List<UserResponseModel> Investors { get; set; }
    }
}
