﻿using Mapster;
using TerraStake.Application.Exceptions;
using TerraStake.Domain.Project;

namespace TerraStake.Application.Project
{
    public class ProjectService : IProjectService
    {

        private readonly IProjectRepository _repository;

        public ProjectService(IProjectRepository repository)
        {
            _repository = repository;
        }

        public async Task CreateAsync(CancellationToken cancellationToken, ProjectRequestModel project)
        {
            var projectToInsert = project.Adapt<ProjectEntity>();

            await _repository.CreateAsync(cancellationToken, projectToInsert);
        }

        public async Task Delete(CancellationToken cancellationToken, int id)
        {
            if (!await _repository.Exists(cancellationToken, id))
                throw new ProjectNotFoundException(id.ToString());

            await _repository.DeleteAsync(cancellationToken, id);
        }

        public async Task<ProjectResponseModel> Get(CancellationToken cancellationToken, int id)
        {
            var result = await _repository.GetAsync(cancellationToken, id);

            if (result == null)
                throw new ProjectNotFoundException(id.ToString());
            else
                return result.Adapt<ProjectResponseModel>();
        }

        public async Task<List<ProjectResponseModel>> GetAll(CancellationToken cancellationToken)
        {
            var result = await _repository.GetAllAsync(cancellationToken);

            return result.Adapt<List<ProjectResponseModel>>();
        }

        public async Task Update(CancellationToken cancellationToken, ProjectRequestModel project)
        {
            if (!await _repository.Exists(cancellationToken, project.ProjectId))
                throw new ProjectNotFoundException(project.ProjectId.ToString());

            var projectToUpdate = project.Adapt<ProjectEntity>();

            await _repository.UpdateAsync(cancellationToken, projectToUpdate);
        }

    }
}
