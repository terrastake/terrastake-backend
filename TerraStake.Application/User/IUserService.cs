﻿namespace TerraStake.Application.User
{
    public interface IUserService
    {
        Task CreateAsync(CancellationToken cancellationToken, UserCreateModel user);
        Task<List<UserResponseModel>> GetAll(CancellationToken cancellationToken);
        Task<UserResponseModel> Get(CancellationToken cancellationToken, int id);
        Task Update(CancellationToken cancellationToken, UserRequestModel user);
        Task Delete(CancellationToken cancellationToken, int id);
    }
}
