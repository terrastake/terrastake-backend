﻿namespace TerraStake.Application.User
{
    public class UserCreateModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
