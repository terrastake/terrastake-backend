﻿using System.Linq.Expressions;
using TerraStake.Domain.User;

namespace TerraStake.Application.User
{
    public interface IUserRepository
    {
        Task<List<UserEntity>> GetAllAsync(CancellationToken cancellationToken);
        Task<List<UserEntity>> GetAllAsync(CancellationToken token, Expression<Func<UserEntity, bool>> func);
        Task<UserEntity> GetAsync(CancellationToken cancellationToken, int id);
        Task UpdateAsync(CancellationToken cancellationToken, UserEntity user);
        Task DeleteAsync(CancellationToken cancellationToken, int id);
        Task<bool> Exists(CancellationToken cancellationToken, int id);
        Task<UserEntity> GetAsync(CancellationToken cancellationToken, string email);
        Task CreateAsync(CancellationToken cancellationToken, UserEntity user);
    }
}