﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TerraStake.Domain.ProjectInvestor;

namespace TerraStake.Domain.User
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Email).IsRequired().HasMaxLength(50);
            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.Password).IsUnicode(false).IsRequired().HasMaxLength(50);

            // Configure the one-to-many relationship for Properties
            builder.HasMany(x => x.Properties)
                .WithOne(x => x.Owner)
                .HasForeignKey(x => x.OwnerId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany(u => u.Investitions)
                .WithMany(p => p.Investors)
                .UsingEntity<ProjectInvestorEntity>(
                    j => j
                        .HasOne(pi => pi.Project)
                        .WithMany()
                        .HasForeignKey(pi => pi.ProjectId)
                        .OnDelete(DeleteBehavior.Cascade),
                    j => j
                        .HasOne(pi => pi.User)
                        .WithMany()
                        .HasForeignKey(pi => pi.UserId)
                        .OnDelete(DeleteBehavior.Cascade),
                    j =>
                    {
                        j.HasKey(pi => new { pi.UserId, pi.ProjectId });
                        j.ToTable("ProjectInvestors"); // Optional: Specify the name of the join table
                    }
                );
        }
    }
}
