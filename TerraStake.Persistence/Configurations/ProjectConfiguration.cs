﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TerraStake.Domain.Project;
using TerraStake.Domain.ProjectInvestor;
using TerraStake.Domain.User;

namespace TerraStake.Infrastructure.Data.Configurations
{
    public class ProjectConfiguration : IEntityTypeConfiguration<ProjectEntity>
    {
        public void Configure(EntityTypeBuilder<ProjectEntity> builder)
        {
            builder.HasKey(x => x.ProjectId);

            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.Cost).IsRequired().HasColumnType("decimal(18,2)");
            builder.Property(x => x.Location).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Area).IsRequired();

            // Configure Images as a JSON column if using EF Core 5.0 or later
            builder.Property(x => x.Images)
                .HasConversion(
                    v => string.Join(';', v),
                    v => v.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList()
                );

            builder
                .HasMany(p => p.Investors)
                .WithMany(u => u.Investitions)
                .UsingEntity<ProjectInvestorEntity>(
                    j => j
                        .HasOne(pi => pi.User)
                        .WithMany()
                        .HasForeignKey(pi => pi.UserId)
                        .OnDelete(DeleteBehavior.Restrict), // Restrict cascade delete here
                    j => j
                        .HasOne(pi => pi.Project)
                        .WithMany()
                        .HasForeignKey(pi => pi.ProjectId)
                        .OnDelete(DeleteBehavior.Cascade), // Cascade delete from ProjectEntity side
                    j =>
                    {
                        j.HasKey(pi => new { pi.ProjectId, pi.UserId });
                        j.ToTable("ProjectInvestors"); // Optional: Specify the name of the join table
                    }
               );
        }
    }
}