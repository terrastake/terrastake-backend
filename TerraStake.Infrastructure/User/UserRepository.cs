﻿using System.Linq.Expressions;
using TerraStake.Application.User;
using TerraStake.Domain.User;
using TerraStake.Persistence.Context;

namespace TerraStake.Infrastructure.User
{
    public class UserRepository : BaseRepository<UserEntity>, IUserRepository
    {

        public UserRepository(TerraStakeContext context) : base(context)
        {

        }

        public async Task<UserEntity> GetAsync(CancellationToken cancellationToken, int id)
        {
            return await base.GetAsync(cancellationToken, id);
        }

        public async Task<List<UserEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await base.GetAllAsync(cancellationToken);
        }

        public async Task<List<UserEntity>> GetAllAsync(CancellationToken cancellation, Expression<Func<UserEntity, bool>> func)
        {
            return await base.GetAllAsync(cancellation, func);
        }

        public async Task CreateAsync(CancellationToken cancellationToken, UserEntity user)
        {
            await AddAsync(cancellationToken, user);
        }

        public async Task UpdateAsync(CancellationToken cancellationToken, UserEntity user)
        {
            await base.UpdateAsync(cancellationToken, user);
        }

        public async Task DeleteAsync(CancellationToken cancellationToken, int id)
        {
            await RemoveAsync(cancellationToken, id);
        }

        public async Task<bool> Exists(CancellationToken cancellationToken, int id)
        {
            return await AnyAsync(cancellationToken, x => x.Id == id);
        }

        public async Task<UserEntity> GetAsync(CancellationToken cancellationToken, string email)
        {
            return await GetAsync(cancellationToken, _ => _.Email.Equals(email));
        }

    }
}
