﻿using System.Linq.Expressions;
using TerraStake.Application.Project;
using TerraStake.Domain.Project;
using TerraStake.Persistence.Context;

namespace TerraStake.Infrastructure.Project
{
    public class ProjectRepository : BaseRepository<ProjectEntity>, IProjectRepository
    {

        public ProjectRepository(TerraStakeContext context) : base(context)
        {

        }

        public async Task<ProjectEntity> GetAsync(CancellationToken cancellationToken, int id)
        {
            return await base.GetAsync(cancellationToken, id);
        }

        public async Task<List<ProjectEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await base.GetAllAsync(cancellationToken);
        }

        public async Task<List<ProjectEntity>> GetAllAsync(CancellationToken cancellation, Expression<Func<ProjectEntity, bool>> func)
        {
            return await base.GetAllAsync(cancellation, func);
        }

        public async Task CreateAsync(CancellationToken cancellationToken, ProjectEntity project)
        {
            await AddAsync(cancellationToken, project);
        }

        public async Task UpdateAsync(CancellationToken cancellationToken, ProjectEntity project)
        {
            await base.UpdateAsync(cancellationToken, project);
        }

        public async Task DeleteAsync(CancellationToken cancellationToken, int id)
        {
            await RemoveAsync(cancellationToken, id);
        }

        public async Task<bool> Exists(CancellationToken cancellationToken, int id)
        {
            return await AnyAsync(cancellationToken, x => x.ProjectId == id);
        }

    }
}

